#include "kreator.h"
#include "ui_kreator.h"
#include "ustawrzuty.h"
#include <QDebug>
#include <QGridLayout>
#include <QPushButton>
#include <QTableWidget>
#include <QHeaderView>
#include <QPushButton>
#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QString>
#include <QLineEdit>
#include <QComboBox>
#include <QBoxLayout>
#include <QCheckBox>
#include <QSpinBox>
#include <QComboBox>
#include <QPlainTextEdit>
#include <QByteArray>
#include <QFile>
#include <QFileDialog>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QList>
#include <QMultiMap>


Kreator::Kreator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Kreator)
{
    ui->setupUi(this);
}

Kreator::~Kreator()
{
    delete ui;
}


void Kreator::on_actionSegment_triggered()//tworzy segment
{
    auto *segment = new QGridLayout;

    auto *buttonUsun = new QPushButton;
    auto *buttonDodaj = new QPushButton;
    buttonUsun->setText("x");
    buttonUsun->setMaximumWidth(25);
    buttonDodaj->setText("+");
    buttonDodaj->setMaximumWidth(25);
    connect(buttonUsun, SIGNAL(clicked()), this, SLOT(usunSegment()));
    connect(buttonDodaj, SIGNAL(clicked()), this, SLOT(dodajPole()));
    buttonUsun->setObjectName(QString::number(ileSeg));
    buttonDodaj->setObjectName(QString::number(ileSeg));
    mapaSegmentow.insert(ileSeg, segment);


    auto *tytul = new QLineEdit;
    tytul->setPlaceholderText("Tytuł segmentu");
    titlesMap.insert(ileSeg, tytul);

    auto *wyborDodawania = new QComboBox;
    wyborDodawania->setObjectName(QString::number(ileSeg));
    wyborDodawania->addItem("Pole tekstowe (linia)");
    wyborDodawania->addItem("Pole tekstowe (box)");
    wyborDodawania->addItem("Pole numeryczne");
    wyborDodawania->addItem("Przycisk rzutu");
    wyborDodawania->addItem("Pole z listą wyborów");
    comboBoxMapWybor.insert(ileSeg, wyborDodawania);

    auto *czyCheckBox = new QCheckBox;
    auto *czyCheckBoxLable = new QLabel;
    czyCheckBoxLable->setText("Dodaj pole wyboru");
    checkBoxMap.insert(ileSeg, czyCheckBox);

    segment->addWidget(tytul,1,0,1,5);
    segment->addWidget(buttonDodaj,2,3);
    segment->addWidget(buttonUsun,0,5);
    segment->addWidget(wyborDodawania,2,2);
    segment->addWidget(czyCheckBoxLable,2,0);
    segment->addWidget(czyCheckBox,2,1);
    ui->verticalLayoutSeg->addLayout(segment);
    ileSeg++;
}


void Kreator::usunSegment()//usuwa segment
{
    QPushButton *btn = qobject_cast<QPushButton*>(sender());

    int ktorySeg = btn->objectName().toInt();
    QGridLayout *segment = mapaSegmentow.value(ktorySeg);

    mapaSegmentow.remove(ktorySeg);
    titlesMap.remove(ktorySeg);

    while(segment->count() > 0)
    {
        QLayoutItem *item = segment->takeAt(0);
        QWidget* widget = item->widget();
        if(widget)
            delete widget;
        delete item;
    }
    delete segment;
}

void Kreator::dodajPole()
{
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    int ktorySeg = btn->objectName().toInt();
    QComboBox *wyborDodawania = comboBoxMapWybor.value(ktorySeg);
    QGridLayout *segment = mapaSegmentow.value(ktorySeg);
    int zaznaczony = wyborDodawania->currentIndex();
    switch(zaznaczony)
    {
        case 0:
        dodajPoleTekstoweLinia(segment, ktorySeg, "", "", false);
        break;

        case 1:
        dodajPoleTekstoweBox(segment, ktorySeg, "", "", false);
        break;

        case 2:
        dodajPoleNumeryczne(segment, ktorySeg, "", "", false);
        break;

        case 3:
        dodajPrzycisk(segment, ktorySeg, "", "", false);
        break;

        case 4:
        dodajPoleListyWyboru(segment, ktorySeg, "", "", false);
        break;
    }
}

void Kreator::dodajPoleTekstoweLinia(QGridLayout *segment, int ktorySeg, QString etykietaValue, QString poleTekstoweValue, bool isChecked)
{
    auto *etykieta = new QLineEdit;
    etykieta->setPlaceholderText("Etykieta");
    etykieta->setMaximumWidth(150);
    etykieta->setText(etykietaValue);

    auto *poleTekstowe = new QLineEdit;
    poleTekstowe->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    poleTekstowe->setText(poleTekstoweValue);

    auto *buttonUsun = new QPushButton;
    buttonUsun->setText("x");
    buttonUsun->setMaximumWidth(25);

    auto *czyCheckBox = checkBoxMap.value(ktorySeg); //do usuniecia ze wszystkich

    int row = segment->rowCount();

    DaneZWiersza daneZWiersza;
    daneZWiersza.typWiersza = 0;
    daneZWiersza.segment = ktorySeg;
    daneZWiersza.wiersz = row;
    daneZWiersza.etykieta = etykieta;
    daneZWiersza.poleTekstowe = poleTekstowe;

    if(czyCheckBox->isChecked())
    {
        auto *checkBox = new QCheckBox;
        checkBox->setChecked(isChecked);
        daneZWiersza.checkBox = checkBox;

        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleTekstowe, row, 1, 1, 2);
        segment->addWidget(checkBox, row, 3);
        segment->addWidget(buttonUsun, row, 4);
        connect(buttonUsun, &QPushButton::pressed,
            [buttonUsun, poleTekstowe, checkBox, etykieta, &listaWierszy = listaWierszy, daneZWiersza]()
            {
                delete etykieta;
                delete buttonUsun;
                delete poleTekstowe;
                delete checkBox;
                for(int i = 0; i < listaWierszy.count(); i++)
                {
                    if(listaWierszy.at(i).wiersz == daneZWiersza.wiersz && listaWierszy.at(i).segment == daneZWiersza.segment)
                    {
                        listaWierszy.removeAt(i);
                        break;
                    }
                }
            }
        );
    }
    else
    {
        daneZWiersza.checkBox = NULL;
        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleTekstowe, row, 1, 1, 3);
        segment->addWidget(buttonUsun, row, 4);
        connect(buttonUsun, &QPushButton::pressed,
            [buttonUsun, poleTekstowe, etykieta, &listaWierszy = listaWierszy, daneZWiersza]()
            {
                delete etykieta;
                delete buttonUsun;
                delete poleTekstowe;
                for(int i = 0; i < listaWierszy.count(); i++)
                {
                    if(listaWierszy.at(i).wiersz == daneZWiersza.wiersz && listaWierszy.at(i).segment == daneZWiersza.segment)
                    {
                        listaWierszy.removeAt(i);
                        break;
                    }
                }
            }
        );
    }
    listaWierszy.push_back(daneZWiersza);
}

void Kreator::dodajPoleTekstoweBox(QGridLayout *segment, int ktorySeg, QString etykietaValue, QString poleTekstoweValue, bool isChecked)
{
    auto *etykieta = new QLineEdit;
    etykieta->setPlaceholderText("Etykieta");
    etykieta->setMaximumWidth(150);
    etykieta->setText(etykietaValue);

    auto *poleTekstowe = new QPlainTextEdit;
    poleTekstowe->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    poleTekstowe->insertPlainText(poleTekstoweValue);

    auto *buttonUsun = new QPushButton;
    buttonUsun->setText("x");
    buttonUsun->setMaximumWidth(25);

    auto *czyCheckBox = checkBoxMap.value(ktorySeg);

    int row = segment->rowCount();

    DaneZWiersza daneZWiersza;
    daneZWiersza.typWiersza = 1;
    daneZWiersza.segment = ktorySeg;
    daneZWiersza.wiersz = row;
    daneZWiersza.etykieta = etykieta;
    daneZWiersza.textBox = poleTekstowe;

    if(czyCheckBox->isChecked())
    {
        auto *checkBox = new QCheckBox;
        checkBox->setChecked(isChecked);
        daneZWiersza.checkBox = checkBox;

        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleTekstowe, row, 1, 1, 2);
        segment->addWidget(checkBox, row, 3);
        segment->addWidget(buttonUsun, row, 4);
        connect(buttonUsun, &QPushButton::pressed,
            [buttonUsun, poleTekstowe, checkBox, etykieta, &listaWierszy = listaWierszy, daneZWiersza]()
            {
                delete etykieta;
                delete buttonUsun;
                delete poleTekstowe;
                delete checkBox;
                for(int i = 0; i < listaWierszy.count(); i++)
                {
                    if(listaWierszy.at(i).wiersz == daneZWiersza.wiersz && listaWierszy.at(i).segment == daneZWiersza.segment)
                    {
                        listaWierszy.removeAt(i);
                        break;
                    }
                }
            }
        );
    }
    else
    {
        daneZWiersza.checkBox = NULL;
        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleTekstowe, row, 1, 1, 3);
        segment->addWidget(buttonUsun, row, 4);
        connect(buttonUsun, &QPushButton::pressed,
            [buttonUsun, poleTekstowe, etykieta, &listaWierszy = listaWierszy, daneZWiersza]()
            {
                delete etykieta;
                delete buttonUsun;
                delete poleTekstowe;
                for(int i = 0; i < listaWierszy.count(); i++)
                {
                    if(listaWierszy.at(i).wiersz == daneZWiersza.wiersz && listaWierszy.at(i).segment == daneZWiersza.segment)
                    {
                        listaWierszy.removeAt(i);
                        break;
                    }
                }
            }
        );
    }
    listaWierszy.push_back(daneZWiersza);
}

void Kreator::dodajPoleNumeryczne(QGridLayout *segment, int ktorySeg, QString etykietaValue, QString poleNumeryczneValue, bool isChecked)
{
    auto *etykieta = new QLineEdit;
    etykieta->setPlaceholderText("Etykieta");
    etykieta->setMaximumWidth(150);
    etykieta->setText(etykietaValue);

    auto *poleNumeryczne = new QSpinBox;
    poleNumeryczne->setMaximum(2147483647);
    poleNumeryczne->setMinimum(-2147483648);
    if(poleNumeryczneValue != "")
    {
        poleNumeryczne->setValue(poleNumeryczneValue.toInt());
    }

    int row = segment->rowCount();

    auto *czyCheckBox = checkBoxMap.value(ktorySeg);

    auto *buttonUsun = new QPushButton;
    buttonUsun->setText("x");
    buttonUsun->setMaximumWidth(25);

    DaneZWiersza daneZWiersza;
    daneZWiersza.typWiersza = 2;
    daneZWiersza.segment = ktorySeg;
    daneZWiersza.wiersz = row;
    daneZWiersza.etykieta = etykieta;
    daneZWiersza.poleNumeryczne = poleNumeryczne;

    if(czyCheckBox->isChecked())
    {
        auto *checkBox = new QCheckBox;
        checkBox->setChecked(isChecked);
        daneZWiersza.checkBox = checkBox;

        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleNumeryczne, row, 1, 1, 2);
        segment->addWidget(checkBox, row, 3);
        segment->addWidget(buttonUsun, row, 4);
        connect(buttonUsun, &QPushButton::pressed,
            [buttonUsun, poleNumeryczne, checkBox, etykieta, &listaWierszy = listaWierszy, daneZWiersza]()
            {
                delete etykieta;
                delete buttonUsun;
                delete poleNumeryczne;
                delete checkBox;
                for(int i = 0; i < listaWierszy.count(); i++)
                {
                    if(listaWierszy.at(i).wiersz == daneZWiersza.wiersz && listaWierszy.at(i).segment == daneZWiersza.segment)
                    {
                        listaWierszy.removeAt(i);
                        break;
                    }
                }
            }
        );
    }
    else
    {
        daneZWiersza.checkBox = NULL;
        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleNumeryczne, row, 1, 1, 3);
        segment->addWidget(buttonUsun, row, 4);
        connect(buttonUsun, &QPushButton::pressed,
            [buttonUsun, poleNumeryczne, etykieta, &listaWierszy = listaWierszy, daneZWiersza]()
            {
                delete etykieta;
                delete buttonUsun;
                delete poleNumeryczne;
                for(int i = 0; i < listaWierszy.count(); i++)
                {
                    if(listaWierszy.at(i).wiersz == daneZWiersza.wiersz && listaWierszy.at(i).segment == daneZWiersza.segment)
                    {
                        listaWierszy.removeAt(i);
                        break;
                    }
                }
            }
        );
    }
    listaWierszy.push_back(daneZWiersza);
}

void Kreator::dodajPrzycisk(QGridLayout *segment, int ktorySeg, QString etykietaValue, QString rzut, bool isChecked)
{
    auto *etykieta = new QLineEdit;
    etykieta->setPlaceholderText("Etykieta");
    etykieta->setMaximumWidth(150);
    etykieta->setText(etykietaValue);

    //to chyba do zmiany jednak będzie
    auto *rollButton = new QPushButton;
    rollButton->setText("roll");
    //connect(rollButton, &QPushButton::pressed,
    //        []()
    //            {
    //                auto *ustawrzut = new UstawRzuty();
    //                ustawrzut->show();
    //            });


    int row = segment->rowCount();

    auto *czyCheckBox = checkBoxMap.value(ktorySeg);

    auto *buttonUsun = new QPushButton;
    buttonUsun->setText("x");
    buttonUsun->setMaximumWidth(25);

    DaneZWiersza daneZWiersza;
    daneZWiersza.typWiersza = 3;
    daneZWiersza.segment = ktorySeg;
    daneZWiersza.wiersz = row;
    daneZWiersza.etykieta = etykieta;
    daneZWiersza.rollPushButton = rollButton;
    daneZWiersza.rzut = rzut;

    if(czyCheckBox->isChecked())
    {
        auto *checkBox = new QCheckBox;
        checkBox->setChecked(isChecked);
        daneZWiersza.checkBox = checkBox;

        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(rollButton, row, 1);
        segment->addWidget(checkBox, row, 3);
        segment->addWidget(buttonUsun, row, 4);
        connect(buttonUsun, &QPushButton::pressed,
            [buttonUsun, checkBox, etykieta, &listaWierszy = listaWierszy, daneZWiersza, rollButton]()
            {
                delete etykieta;
                delete buttonUsun;
                delete checkBox;
                delete rollButton;
                for(int i = 0; i < listaWierszy.count(); i++)
                {
                    if(listaWierszy.at(i).wiersz == daneZWiersza.wiersz && listaWierszy.at(i).segment == daneZWiersza.segment)
                    {
                        listaWierszy.removeAt(i);
                        break;
                    }
                }
            }
        );
    }
    else
    {
        daneZWiersza.checkBox = NULL;
        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(rollButton, row, 2);
        segment->addWidget(buttonUsun, row, 4);
        connect(buttonUsun, &QPushButton::pressed,
            [buttonUsun, etykieta, &listaWierszy = listaWierszy, daneZWiersza, rollButton]()
            {
                delete etykieta;
                delete buttonUsun;
                delete rollButton;
                for(int i = 0; i < listaWierszy.count(); i++)
                {
                    if(listaWierszy.at(i).wiersz == daneZWiersza.wiersz && listaWierszy.at(i).segment == daneZWiersza.segment)
                    {
                        listaWierszy.removeAt(i);
                        break;
                    }
                }
            }
        );
    }
    listaWierszy.push_back(daneZWiersza);
}

void Kreator::dodajPoleListyWyboru(QGridLayout *segment, int ktorySeg, QString etykietaValue, QString poleListyWyboruValue, bool isChecked)
{
    auto *etykieta = new QLineEdit;
    etykieta->setPlaceholderText("Etykieta");
    etykieta->setMaximumWidth(150);
    etykieta->setText(etykietaValue);

    auto *poleListyWyboru = new QComboBox;
    poleListyWyboru->setEditable(true);
    if(poleListyWyboruValue != "")
    {
        QStringList itemsList = poleListyWyboruValue.split("\n");
        poleListyWyboru->addItems(itemsList);
    }
    poleListyWyboru->setPlaceholderText("wpisz wartość i zatwierdź dodanie spacją");

    auto *buttonUsunZListyWyboru = new QPushButton;
    buttonUsunZListyWyboru->setText("usun z listy");
    connect(buttonUsunZListyWyboru, &QPushButton::pressed,
        [poleListyWyboru]()
        {
            poleListyWyboru->removeItem(poleListyWyboru->currentIndex());
        }
    );


    int row = segment->rowCount();

    auto *czyCheckBox = checkBoxMap.value(ktorySeg);

    auto *buttonUsun = new QPushButton;
    buttonUsun->setText("x");
    buttonUsun->setMaximumWidth(25);

    DaneZWiersza daneZWiersza;
    daneZWiersza.typWiersza = 4;
    daneZWiersza.segment = ktorySeg;
    daneZWiersza.wiersz = row;
    daneZWiersza.etykieta = etykieta;
    daneZWiersza.comboBox = poleListyWyboru;

    if(czyCheckBox->isChecked())
    {
        auto *checkBox = new QCheckBox;
        checkBox->setChecked(isChecked);
        daneZWiersza.checkBox = checkBox;

        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleListyWyboru, row, 1, 1, 1);
        segment->addWidget(checkBox, row, 3);
        segment->addWidget(buttonUsunZListyWyboru, row, 2);
        segment->addWidget(buttonUsun, row, 4);
        connect(buttonUsun, &QPushButton::pressed,
            [buttonUsun, poleListyWyboru, checkBox, etykieta, buttonUsunZListyWyboru, &listaWierszy = listaWierszy, daneZWiersza]()
            {
                delete etykieta;
                delete buttonUsun;
                delete poleListyWyboru;
                delete checkBox;
                delete buttonUsunZListyWyboru;
                for(int i = 0; i < listaWierszy.count(); i++)
                {
                    if(listaWierszy.at(i).wiersz == daneZWiersza.wiersz && listaWierszy.at(i).segment == daneZWiersza.segment)
                    {
                        listaWierszy.removeAt(i);
                        break;
                    }
                }
            }
        );
    }
    else
    {
        daneZWiersza.checkBox = NULL;
        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleListyWyboru, row, 1, 1, 2);
        segment->addWidget(buttonUsunZListyWyboru, row, 3);
        segment->addWidget(buttonUsun, row, 4);
        connect(buttonUsun, &QPushButton::pressed,
            [buttonUsun, poleListyWyboru, etykieta, buttonUsunZListyWyboru, &listaWierszy = listaWierszy, daneZWiersza]()
            {
                delete etykieta;
                delete buttonUsun;
                delete poleListyWyboru;
                delete buttonUsunZListyWyboru;
                for(int i = 0; i < listaWierszy.count(); i++)
                {
                    if(listaWierszy.at(i).wiersz == daneZWiersza.wiersz && listaWierszy.at(i).segment == daneZWiersza.segment)
                    {
                        listaWierszy.removeAt(i);
                        break;
                    }
                }
            }
        );
    }
    listaWierszy.push_back(daneZWiersza);
}

void Kreator::on_actionZapisz_triggered()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Xml"), "./karta.xml",
                                                    tr("XML files (*.xml)"));
    QFile file(filename);
    if(file.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter xmlWriter(&file);
        xmlWriter.setAutoFormatting(true);
        xmlWriter.writeStartDocument();
        xmlWriter.writeStartElement("karta");
        for(int i = 0; i < ileSeg; i++)
        {
            if(titlesMap.value(i) != NULL)
            {
                xmlWriter.writeStartElement("segment");
                xmlWriter.writeAttribute("tytul",titlesMap.value(i)->text());
                for(auto wiersz : listaWierszy)
                {
                    if(wiersz.segment == i)
                    {
                        xmlWriter.writeStartElement("wiersz");
                        int typWiersza = wiersz.typWiersza;
                        QLineEdit *etykieta = wiersz.etykieta;

                        xmlWriter.writeAttribute("typ", QString::number(typWiersza));
                        xmlWriter.writeTextElement("etykieta", etykieta->text());
                        if(wiersz.checkBox != NULL)
                        {
                            bool checkBoxChecked = wiersz.checkBox->isChecked();
                            xmlWriter.writeStartElement("checkBox");
                            xmlWriter.writeAttribute("czyCheckBox", QString::number(1));
                            xmlWriter.writeAttribute("isChecked", QString::number(checkBoxChecked));
                            xmlWriter.writeEndElement();
                        }
                        else
                        {
                            xmlWriter.writeStartElement("checkBox");
                            xmlWriter.writeAttribute("czyCheckBox", QString::number(0));
                            xmlWriter.writeAttribute("isChecked", QString::number(0));
                            xmlWriter.writeEndElement();
                        }

                        switch(typWiersza)
                        {
                        case 0:
                            xmlWriter.writeTextElement("poleTekstowe", wiersz.poleTekstowe->text());
                        break;

                        case 1:
                            xmlWriter.writeTextElement("textBox", wiersz.textBox->toPlainText());
                        break;

                        case 2:
                            xmlWriter.writeStartElement("poleNum");
                            xmlWriter.writeAttribute("kod", "[s:" + QString::number(wiersz.segment) + " w:" + QString::number(wiersz.wiersz) + "]");
                            xmlWriter.writeTextElement("poleNumeryczne", QString::number(wiersz.poleNumeryczne->value()));
                            xmlWriter.writeEndElement();
                        break;

                        case 3:
                            xmlWriter.writeTextElement("przycisk", wiersz.rzut);
                        break;

                        case 4:
                            QComboBox *comboBox = wiersz.comboBox;
                            QString itemsText = "";
                            for(int x = 0; x<comboBox->count(); x++)
                            {
                                itemsText += comboBox->itemText(x) + "\n";
                            }
                            xmlWriter.writeTextElement("comboBox", itemsText);
                        break;
                        }

                        xmlWriter.writeEndElement();
                    }
                }
                xmlWriter.writeEndElement();
            }
        }
        xmlWriter.writeEndElement();
        file.close();
    }
}

void Kreator::on_actionOtw_rz_triggered()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("Open Xml"), "./karta.xml",
                                                    tr("XML files (*.xml)"));
    QFile file(filename);
    if(file.open(QIODevice::ReadOnly))
    {
        QString etykietaValue = "";
        QXmlStreamReader Rxml;
        bool isChecked = false;
        Rxml.setDevice(&file);
        czyszczeniePrzedWczytaniem();
        while(!Rxml.atEnd())
        {
            if(Rxml.isStartElement())
            {
                if(Rxml.name().toString() == "etykieta")
                {
                    etykietaValue = Rxml.readElementText();
                }
                else if(Rxml.name().toString() == "checkBox")
                {
                    isChecked =Rxml.attributes().value("isChecked").toString().toInt();
                    checkBoxMap.value(ileSeg-1)->setChecked(Rxml.attributes().value("czyCheckBox").toString().toInt());
                }
                else if(Rxml.name().toString() == "poleTekstowe") //jeśli linia tekstu
                {
                    QGridLayout *segment = mapaSegmentow.value(ileSeg-1);
                    dodajPoleTekstoweLinia(segment, ileSeg-1, etykietaValue, Rxml.readElementText(), isChecked);
                }
                else if(Rxml.name().toString() == "textBox") //jeśli tekst box
                {
                    QGridLayout *segment = mapaSegmentow.value(ileSeg-1);
                    dodajPoleTekstoweBox(segment, ileSeg-1, etykietaValue, Rxml.readElementText(), isChecked);
                }
                else if(Rxml.name().toString() == "poleNumeryczne") //jeśli pole numeryczne
                {
                    QGridLayout *segment = mapaSegmentow.value(ileSeg-1);
                    dodajPoleNumeryczne(segment, ileSeg-1, etykietaValue, Rxml.readElementText(), isChecked);
                }
                else if(Rxml.name().toString() == "przycisk") //jeśli pole numeryczne z przyciskiem
                {
                    QGridLayout *segment = mapaSegmentow.value(ileSeg-1);
                    dodajPrzycisk(segment, ileSeg-1, etykietaValue, Rxml.readElementText(), isChecked);
                }
                else if(Rxml.name().toString() == "comboBox")//jeśli pole z listą
                {
                    QGridLayout *segment = mapaSegmentow.value(ileSeg-1);
                    dodajPoleListyWyboru(segment, ileSeg-1, etykietaValue, Rxml.readElementText(), isChecked);
                }
                else if(Rxml.name().toString() == "segment")
                {
                    on_actionSegment_triggered();
                    titlesMap.value(ileSeg-1)->setText(Rxml.attributes().value("tytul").toString());
                }
            }
            Rxml.readNext();
        }
    }
    file.close();
}

void Kreator::czyszczeniePrzedWczytaniem()
{
    if(ileSeg != 0)
    {
        for (auto mapa = mapaSegmentow.begin(); mapa != mapaSegmentow.end(); ++mapa)
        {
            QGridLayout *segment = mapa.value();
            while(segment->count() > 0)
            {
                QLayoutItem *item = segment->takeAt(0);
                QWidget* widget = item->widget();
                if(widget)
                    delete widget;
                delete item;
            }
            delete segment;
        }

        //zerowanie zmiennych
        mapaSegmentow.clear();
        checkBoxMap.clear();
        comboBoxMapWybor.clear();
        titlesMap.clear();
        ileSeg = 0;
        listaWierszy.clear();
    }
}

void Kreator::on_actionRzuty_triggered()
{
    UstawRzuty *ustawRzuty = new UstawRzuty;
    for(int i = 0; i < ileSeg; i++)
    {
        if(titlesMap.value(i) != NULL)
        {
            for(auto wiersz : listaWierszy)
            {
                if(wiersz.segment == i)
                {
                    if(wiersz.typWiersza == 3 || wiersz.typWiersza == 2)
                    {
                        ustawRzuty->doajWiersze(wiersz.typWiersza, wiersz.segment, titlesMap.value(wiersz.segment)->text(), wiersz.wiersz, wiersz.etykieta, wiersz.poleTekstowe, wiersz.rollPushButton, wiersz.poleNumeryczne, wiersz.rzut);
                    }
                }

            }
        }
    }
    ustawRzuty->ustawTable();
    ustawRzuty->show();
    connect(ustawRzuty, &UstawRzuty::updateRzuty, [=](QMultiMap <int, QMap<int, QString>> mapaZRzutami)
    {
        for (QMultiMap<int, QMap<int, QString>>::const_iterator it = mapaZRzutami.cbegin(), end = mapaZRzutami.cend(); it != end; ++it) {
            qDebug() << "The key: " << it.key() << Qt::endl;
            qDebug() << "The value: " << it.value() << Qt::endl;
            for(int i = 0; i < listaWierszy.size(); i++)
            {
                if(it.key() == listaWierszy.at(i).segment)
                {
                    if(it.value().value(1).toInt() == listaWierszy.at(i).wiersz)
                    {
                        DaneZWiersza daneZWiersza;
                        daneZWiersza.typWiersza = listaWierszy.at(i).typWiersza;
                        daneZWiersza.segment = listaWierszy.at(i).segment;
                        daneZWiersza.wiersz = listaWierszy.at(i).wiersz;
                        daneZWiersza.etykieta = listaWierszy.at(i).etykieta;
                        daneZWiersza.rollPushButton = listaWierszy.at(i).rollPushButton;
                        daneZWiersza.rzut = it.value().value(0);
                        listaWierszy.removeAt(i);
                        listaWierszy.push_back(daneZWiersza);
                    }
                }
            }
        }
    });
}
