#ifndef KARTYPOSTACI_H
#define KARTYPOSTACI_H

#include <QMainWindow>

#include <QGridLayout>
#include <QMap>
#include <QLineEdit>
#include <QComboBox>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QSpinBox>
#include <QCheckBox>
#include <QList>
#include <QLabel>
#include "rzuty.h"

namespace Ui {
class KartyPostaci;
}

class KartyPostaci : public QMainWindow
{
    Q_OBJECT

public:
    explicit KartyPostaci(QWidget *parent = nullptr);
    ~KartyPostaci();
    void getRzuty(Rzuty*);
    void otworz(QString);

private slots:
    void import(QString);
    void dodajSegment(QString);
    void dodajPoleTekstoweLinia(QGridLayout*, int, QString, QString, bool, bool);
    void dodajPoleTekstoweBox(QGridLayout*, int, QString, QString, bool, bool);
    void dodajPoleNumeryczne(QGridLayout*, int, QString, QString, bool, bool, QString);
    void dodajPrzycisk(QGridLayout*, int, QString, QString, bool, bool);
    void dodajPoleListyWyboru(QGridLayout*, int, QString, QString, bool, bool);
    void wyczysc();

    void on_actionZapisz_triggered();

    void on_actionOtw_rz_triggered();

private:
    Ui::KartyPostaci *ui;

    int ileSeg = 0;
    QMap<int, QGridLayout *> mapaSegmentow;

    struct DaneZWiersza
    {
        int segment;
        int wiersz;
        int typWiersza;
        QLabel *etykieta;
        QComboBox *comboBox;
        QLineEdit *poleTekstowe;
        QPlainTextEdit *textBox;
        QPushButton *rollPushButton;
        QSpinBox *poleNumeryczne;
        QCheckBox *checkBox;
        QString rzut;
    };

    Rzuty *rzutyPrzypisaneDoKarty;
    QList<DaneZWiersza> listaWierszy;
    QMap<QString, QSpinBox *> mapaPolWartosci;
    QMap<int, QString> mapaTytulow;
};

#endif // KARTYPOSTACI_H
