#ifndef DIALOGPOLACZ_H
#define DIALOGPOLACZ_H

#include <QDialog>
#include <QMap>

namespace Ui {
class DialogPolacz;
}

class DialogPolacz : public QDialog
{
    Q_OBJECT

public:
    explicit DialogPolacz(QWidget *parent = nullptr);
    ~DialogPolacz();

    void wczytajPolaczenia(QStringList);
    QStringList przekażPolaczenia();

private slots:
    void on_pushButton_clicked();

    void dodajPolaczenieKP(QString);
    void dodajPolaczenieDaty(QString);

private:
    Ui::DialogPolacz *ui;
    QMap<int,QString> polaczeniaMapa;
    int polaczenieID = 0;
};

#endif // DIALOGPOLACZ_H
