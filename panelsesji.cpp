#include "panelsesji.h"
#include "ui_panelsesji.h"

#include <QSpacerItem>
#include <QDebug>
#include <QGridLayout>
#include <QPlainTextEdit>
#include <QScrollArea>
#include <QSizePolicy>
#include "dialogpolacz.h"
#include <QFileDialog>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

PanelSesji::PanelSesji(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PanelSesji)
{
    ui->setupUi(this);
    on_tabWidget_tabCloseRequested(0);
    on_tabWidget_tabCloseRequested(0);
}

PanelSesji::~PanelSesji()
{
    delete ui;
}

void PanelSesji::on_tabWidget_tabCloseRequested(int index)
{
    ui->tabWidget->setCurrentIndex(index);
    QMap<int, Zakladka>::iterator it;
    Zakladka zakladka;
    for (it = mapaZakladek.begin(); it != mapaZakladek.end(); ++it)
    {
        zakladka = it.value();
        if(zakladka.tabWidget == ui->tabWidget->currentWidget())
        {
            mapaZakladek.remove(it.key());
            break;
        }
    }
    ui->tabWidget->removeTab(index);
}


void PanelSesji::on_pushButton_clicked() //nowa zakładka
{
    auto widget = new QWidget;
    auto layout = new QVBoxLayout;

    auto widgetElementy = new QWidget;
    auto layoutElementy = new QVBoxLayout;

    widgetElementy->setLayout(layoutElementy);

    auto buttonDodaj = new QPushButton;
    buttonDodaj->setText("+");

    auto scrollArea = new QScrollArea;

    scrollArea->setWidgetResizable(true);
    scrollArea->setWidget(widgetElementy);
    layout->addWidget(scrollArea);
    layout->addWidget(buttonDodaj,0,Qt::AlignRight);

    widget->setLayout(layout);

    ui->tabWidget->addTab(widget, ui->lineEdit->text());

    layoutElementy->setObjectName(QString::number(zakladkaID));
    mapaWidokowKart.insert(buttonDodaj, layoutElementy);

    connect(buttonDodaj, SIGNAL(clicked()), this, SLOT(dodajElement()));

    //utworzenie struktury zakładki i dodanie do mapy zakładek
    Zakladka zakladka;
    QList<ElementZakladki> listaZawartosciElementow;
    zakladka.tabWidget = widget;
    zakladka.tytul = ui->lineEdit->text();
    zakladka.listaZawartosciElementow = listaZawartosciElementow;
    mapaZakladek.insert(zakladkaID,zakladka);

    ui->lineEdit->setText("");
    zakladkaID++;
}

void PanelSesji::dodajElement()
{

    QStringList polaczenia;
    auto widgetElement = new QWidget;
    auto widgetPrzyciski = new QWidget;
    auto layoutPrzyciski = new QHBoxLayout;
    widgetPrzyciski->setLayout(layoutPrzyciski);
    auto layoutElement = new QGridLayout;
    auto notatki = new QPlainTextEdit;
    auto etykieta = new QLineEdit;
    auto buttonPolacz = new QPushButton;
    auto otworzZasob = new QPushButton;
    auto buttonUsun = new QPushButton;
    auto buttonPokazNotatki = new QPushButton;

    notatki->setVisible(false);

    //dodanie do layuoutu
    layoutElement->addWidget(buttonUsun, 0, 5, Qt::AlignRight);
    layoutElement->addWidget(etykieta, 1, 0, Qt::AlignLeft);
    layoutPrzyciski->addWidget(otworzZasob);
    layoutPrzyciski->addWidget(buttonPolacz);
    layoutPrzyciski->addWidget(buttonPokazNotatki);
    layoutElement->addWidget(widgetPrzyciski, 1, 4, 1, 2);
    layoutElement->addWidget(notatki, 2, 0, 1, 6);

    widgetElement->setLayout(layoutElement);

    widgetElement->setMaximumHeight(200);


    //dodanie do odpowiedniego taba
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    QVBoxLayout * glownyLayout;
    if(btn == NULL)
    {
        glownyLayout = mapaWidokowKart.first();
    }
    else
    {
        glownyLayout = mapaWidokowKart.value(btn);
    }
    widgetElement->setMinimumHeight(200);
    glownyLayout->addWidget(widgetElement);

    //utworzenie struktury elementu
    ElementZakladki elementZakladki;
    elementZakladki.etykieta = etykieta;
    elementZakladki.notatki = notatki;
    elementZakladki.polaczenia = polaczenia;

    //aktualizacja mapy zakładek
    Zakladka zakladka = mapaZakladek.value(glownyLayout->objectName().toInt());
    mapaZakladek.remove(glownyLayout->objectName().toInt());
    zakladka.listaZawartosciElementow.append(elementZakladki);
    mapaZakladek.insert(glownyLayout->objectName().toInt(), zakladka);

    //connect przycisków
    int id = glownyLayout->objectName().toInt();
    buttonUsun->setText("x");
    connect(buttonUsun, &QPushButton::pressed,
        [widgetElement, id, &mapaZakladek = mapaZakladek, etykieta]()
        {
            Zakladka zakladka = mapaZakladek.value(id);
            for(int i = 0; i < zakladka.listaZawartosciElementow.size(); i++)
            {
                if(zakladka.listaZawartosciElementow.at(i).etykieta == etykieta)
                {
                    zakladka.listaZawartosciElementow.removeAt(i);
                }
            }
            // zrobić fora z porównaniem do wszystkich
            delete widgetElement;
            mapaZakladek.remove(id);
            mapaZakladek.insert(id, zakladka);
        }
    );

    buttonPokazNotatki->setText("Pokaż notatki");
    connect(buttonPokazNotatki, &QPushButton::pressed,
        [notatki, buttonPokazNotatki]()
        {
            if(notatki->isVisible())
            {
                notatki->setVisible(false);
                buttonPokazNotatki->setText("Pokaż notatki");
            }
            else
            {
                notatki->setVisible(true);
                buttonPokazNotatki->setText("Ukryj notatki");
            }
        }
    );

    buttonPolacz->setText("Połącz zasób");
    connect(buttonPolacz, &QPushButton::pressed,
        [id, &mapaZakladek = mapaZakladek, etykieta]()
        {
            Zakladka zakladka = mapaZakladek.value(id);
            DialogPolacz dialogPolacz;
            for(int i = 0; i < zakladka.listaZawartosciElementow.size(); i++)
            {
                if(zakladka.listaZawartosciElementow.at(i).etykieta == etykieta)
                {
                    dialogPolacz.wczytajPolaczenia(zakladka.listaZawartosciElementow.at(i).polaczenia);
                    if(dialogPolacz.exec() == QDialog::Accepted)
                    {
                        ElementZakladki staryElement = zakladka.listaZawartosciElementow.at(i);
                        ElementZakladki nowyElement;
                        nowyElement.etykieta = staryElement.etykieta;
                        nowyElement.notatki = staryElement.notatki;
                        nowyElement.polaczenia = dialogPolacz.przekażPolaczenia();
                        zakladka.listaZawartosciElementow.replace(i,nowyElement);

                        mapaZakladek.remove(id);
                        mapaZakladek.insert(id, zakladka);
                    }
                }
            }
        }
    );

    otworzZasob->setText("Pokaż połączony zasób");
    connect(otworzZasob, &QPushButton::pressed,
        [id, &mapaZakladek = mapaZakladek, etykieta, this]()
        {
            Zakladka zakladka = mapaZakladek.value(id);
            for(int i = 0; i < zakladka.listaZawartosciElementow.size(); i++)
            {
                if(zakladka.listaZawartosciElementow.at(i).etykieta == etykieta)
                {
                        ElementZakladki element = zakladka.listaZawartosciElementow.at(i);
                        emit this->doOtwarcia(element.polaczenia);
                }
            }
        }
    );


}

void PanelSesji::on_actionOtw_rz_triggered()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("Open Xml"), "./panel.xml",
                                                    tr("XML files (*.xml)"));
    QFile file(filename);
    if(file.open(QIODevice::ReadOnly))
    {
        wyczyscPrzedOtwarciem();

        QXmlStreamReader Rxml;

        Rxml.setDevice(&file);

        while(!Rxml.atEnd())
        {
            if(Rxml.isStartElement())
            {
                if(Rxml.name().toString() == "zakladka")
                {
                    ui->lineEdit->setText(Rxml.attributes().value("tytul").toString());
                    on_pushButton_clicked(); //dodaje zakladke
                    Rxml.readNext();
                    QString etykieta ="";
                    QString notatki ="";
                    QStringList polaczenia;
                    while(Rxml.name().toString() != "zakladka")
                    {
                        if(Rxml.name().toString() == "etykieta") //etykieta
                        {
                            etykieta = Rxml.readElementText();
                        }
                        else if(Rxml.name().toString() == "notatki") //notatka
                        {
                            notatki = Rxml.readElementText();
                        }
                        else if(Rxml.name().toString() == "polaczenie") //polaczenie
                        {
                            polaczenia.append(Rxml.readElementText());
                        }
                        else if(Rxml.name().toString() == "element" && Rxml.isEndElement())
                        {
                            dodajElement();
                            mapaZakladek.last().listaZawartosciElementow.last().etykieta->setText(etykieta);
                            mapaZakladek.last().listaZawartosciElementow.last().notatki->setPlainText(notatki);
                            mapaZakladek.last().listaZawartosciElementow.last().polaczenia.append(polaczenia);
                            etykieta ="";
                            notatki ="";
                            polaczenia.clear();
                        }
                        Rxml.readNext();
                    }
                }

            }
            Rxml.readNext();
        }
        file.close();
    }
}


void PanelSesji::on_actionZapisz_triggered()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Xml"), "./panel.xml",
                                                    tr("XML files (*.xml)"));

    QFile file(filename);
    if(file.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter xmlWriter(&file);
        xmlWriter.setAutoFormatting(true);
        xmlWriter.writeStartDocument();
        xmlWriter.writeStartElement("panel");
        //zapisanie wszystkich tabów i ich elementów
        QMap<int, Zakladka>::iterator it;
        Zakladka zakladka;

        for (it = mapaZakladek.begin(); it != mapaZakladek.end(); ++it)
        {
            zakladka = it.value();
            xmlWriter.writeStartElement("zakladka"); //zakladka start
            xmlWriter.writeAttribute("tytul",zakladka.tytul);
            QList listaElementow = zakladka.listaZawartosciElementow;
            for(auto element : listaElementow)
            {
                xmlWriter.writeStartElement("element"); //element start
                xmlWriter.writeTextElement("etykieta", element.etykieta->text());
                xmlWriter.writeTextElement("notatki", element.notatki->toPlainText());
                for(auto polaczenie : element.polaczenia)
                {
                    xmlWriter.writeTextElement("polaczenie", polaczenie);
                }
                xmlWriter.writeEndElement();// element end
            }
            xmlWriter.writeEndElement(); // zakladka end
        }
        xmlWriter.writeEndDocument();
        file.close();
    }
}

void PanelSesji::wyczyscPrzedOtwarciem()
{

    while(ui->tabWidget->count() != 0)
    {
        on_tabWidget_tabCloseRequested(0);
    }
    zakladkaID = 0;
    mapaWidokowKart.clear();
}
