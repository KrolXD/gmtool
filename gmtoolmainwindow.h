#pragma once

#include <kddockwidgets/DockWidget.h>
#include <kddockwidgets/MainWindow.h>
#include "rzuty.h"

class GmToolMainWindow : public KDDockWidgets::MainWindow
{
    Q_OBJECT
public:
    explicit GmToolMainWindow(const QString &uniqueName, KDDockWidgets::MainWindowOptions options,
                          bool restoreIsRelative,
                          const QString &affinityName = {},
                          QWidget *parent = nullptr);
    ~GmToolMainWindow() override;

private:
    void restoreWidgets();
    KDDockWidgets::DockWidgetBase* newDockWidget();
    QMenu *m_toggleMenu = nullptr;
    const bool m_restoreIsRelative;
    KDDockWidgets::DockWidget::List m_dockwidgets;
    int ileKreator = 0;
    int ileKalkulator = 0;
    int ileRzuty = 0;
    int ileKarty = 0;
    int ileSesji = 0;

    KDDockWidgets::DockWidget * dockRzutyDoKart;
    Rzuty * rzutyDoKart;
private slots:
    void otworzZListy(QStringList);
    void otworzKarte(QString);
    void otworzDaty(QString);
};
