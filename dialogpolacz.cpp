#include "dialogpolacz.h"
#include "ui_dialogpolacz.h"

#include <QPushButton>>
#include <QLineEdit>
#include <QFileDialog>

DialogPolacz::DialogPolacz(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogPolacz)
{
    ui->setupUi(this);
}

DialogPolacz::~DialogPolacz()
{
    delete ui;
}

void DialogPolacz::on_pushButton_clicked()
{
    if(ui->comboBox->currentIndex() == 0)
    {
        dodajPolaczenieKP("");
    }
    else if (ui->comboBox->currentIndex() == 1)
    {
        dodajPolaczenieDaty("");
    }
}

void DialogPolacz::dodajPolaczenieKP(QString polaczenieS)
{
    auto btn = new QPushButton;
    btn->setText("Połącz z kartą postaci");
    btn->setMinimumWidth(150);
    auto polaczenie = new QLineEdit;
    polaczenie->setReadOnly(true);
    polaczenie->setObjectName(QString::number(polaczenieID));
    polaczenie->setText(polaczenieS);
    if(polaczenieS != "")
    {
        polaczeniaMapa.insert(polaczenie->objectName().toInt(), polaczenie->text());
    }

    auto widget = new QWidget;
    auto layout = new QHBoxLayout;

    connect(btn, &QPushButton::pressed,
        [this, polaczenie]()
        {
            QString filename = QFileDialog::getOpenFileName(this,
                                                        tr("Open File"), "./karta.xml",
                                                        tr("XML files (*.xml)"));
            polaczenie->setText(filename);

            polaczenie->setText(filename);

            //dodanie do mapy polaczen
            polaczeniaMapa.insert(polaczenie->objectName().toInt(), polaczenie->text());
        }
    );

    widget->setLayout(layout);
    layout->addWidget(polaczenie);
    layout->addWidget(btn);

    ui->verticalLayout_2->addWidget(widget);
    polaczenieID++;
}

void DialogPolacz::dodajPolaczenieDaty(QString polaczenieS)
{
    auto btn = new QPushButton;
    btn->setText("Połącz z datami");
    btn->setMinimumWidth(150);
    auto polaczenie = new QLineEdit;
    polaczenie->setReadOnly(true);
    polaczenie->setObjectName(QString::number(polaczenieID));
    polaczenie->setText(polaczenieS);
    if(polaczenieS != "")
    {
        polaczeniaMapa.insert(polaczenie->objectName().toInt(), polaczenie->text());
    }


    auto widget = new QWidget;
    auto layout = new QHBoxLayout;

    connect(btn, &QPushButton::pressed,
        [this, polaczenie, &polaczeniaMapa = polaczeniaMapa]()
        {
            QString filename = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                            "./daty.csv",
                                                            tr("CSV files (*.csv)"));
            polaczenie->setText(filename);

            //dodanie do mapy polaczen
            polaczeniaMapa.insert(polaczenie->objectName().toInt(), polaczenie->text());
        }
    );

    widget->setLayout(layout);
    layout->addWidget(polaczenie);
    layout->addWidget(btn);

    ui->verticalLayout_2->addWidget(widget);
    polaczenieID++;
}

QStringList DialogPolacz::przekażPolaczenia()
{
    QStringList polaczenia;
    for(int i = 0; i < polaczenieID; i++)
    {
        polaczenia.append(polaczeniaMapa.value(i));
    }
    return polaczenia;
}

void DialogPolacz::wczytajPolaczenia(QStringList polaczenia)
{
    for(int i = 0; i < polaczenia.size(); i++)
    {
        QString polaczenie = polaczenia.at(i);
        if(polaczenie.contains(".csv"))
        {
            dodajPolaczenieDaty(polaczenie);
        }
        else if(polaczenie.contains(".xml"))
        {
            dodajPolaczenieKP(polaczenie);
        }
    }
}
