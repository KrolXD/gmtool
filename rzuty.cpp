#include "rzuty.h"
#include "ui_rzuty.h"
#include <QRegularExpression>
#include <QRandomGenerator>
#include <QRegularExpressionMatch>
#include <QVector>
#include <QDebug>
#include <QRegularExpressionMatchIterator>
#include "tinyexpr.h"
#include <QList>
#include <QMessageBox>
#include <QLabel>
#include <QRadioButton>
#include <QVBoxLayout>

Rzuty::Rzuty(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Rzuty)
{
    ui->setupUi(this);
}

Rzuty::~Rzuty()
{
    delete ui;
}

bool Rzuty::sprawdzFormat(QString rzut)
{
    QString rzutBezNawiasow = rzut;
    rzutBezNawiasow.replace(QRegularExpression("\\(-"), "(");
    rzutBezNawiasow.remove(QRegularExpression("[\\(\\)]"));
    qDebug()<< rzut;
    QRegularExpression reKosci("^((\\d+d{1}\\d+)|(\\d+)){1}(([\\+\\-\\*\\/]){1}((\\d+d{1}\\d+)|(\\d+)){1})*$");
    QRegularExpressionMatch match = reKosci.match(rzutBezNawiasow);
    if(match.hasMatch())
    {
        double testValue = te_interp(rzut.remove("d").toUtf8(), 0);

        bool moznapoliczyc = (testValue == testValue);
        if(moznapoliczyc)
        {
            return true;
        }
    }
    return false;
}

QStringList Rzuty::wykonajRzut(QString doRzucenia)
{
    QStringList doRzuceniaSplit = doRzucenia.split("d");
    QStringList wynikRzutu; // [0]int wynik [1]w formacie do wyswietlenia [2]wyniki każdej kości [3]zsumowane wyniki kości
    int ilosc = doRzuceniaSplit[0].toInt();
    int kosc = doRzuceniaSplit[1].toInt();
    int suma = 0;
    QString stringWynik = doRzucenia + "[ ";
    QString kosciOsobno = doRzucenia + "[ ";
    QVector <int> sumowaieOczek(kosc,0);

    for(int x = 0; x < ilosc; x++)
    {
        int random = QRandomGenerator::global()->bounded(kosc) + 1;
        suma += random;
        kosciOsobno += QString::number(random) + " ";
        sumowaieOczek[random-1]++;
    }

    for(int x = 0; x < sumowaieOczek.length(); x++)
    {
        if(sumowaieOczek[x] != 0)
        {
            stringWynik += QString::number(x+1) + "x" + QString::number(sumowaieOczek[x]) + " ";
        }
    }

    stringWynik += "]";
    kosciOsobno += "]";

    wynikRzutu.append(QString::number(suma));
    wynikRzutu.append(doRzucenia + "[ " + QString::number(suma) + " ]");
    wynikRzutu.append(kosciOsobno);
    wynikRzutu.append(stringWynik);

    return wynikRzutu;
}

QMap<int, QStringList> Rzuty::rezultatRzutow(QString calosc)
{
    QMap<int, QStringList> mapaWynikow;
    mapaWynikow.insert(0,{"Niepoprawny format"});
    if(sprawdzFormat(calosc))
    {
        QStringList wyniki;
        QString caloscDoObliczen = calosc;
        QList<QStringList> listaWynikow;
        QRegularExpression re("\\d+d{1}\\d+");
        QRegularExpressionMatchIterator matches = re.globalMatch(calosc);
        int roznicaDlugosci = 0;

        for(int i = 1; matches.hasNext(); i++)
        {
            QRegularExpressionMatch match = matches.next();
            wyniki = wykonajRzut(match.captured());
            listaWynikow<<wyniki;
            caloscDoObliczen.replace(match.capturedStart()+roznicaDlugosci, match.capturedLength(), wyniki.at(0));
            roznicaDlugosci += wyniki.at(0).length() - match.capturedLength();
            mapaWynikow.insert(i,wyniki);
        }
        double answer = te_interp(caloscDoObliczen.toUtf8(), 0);
        mapaWynikow.remove(0);
        mapaWynikow.insert(0,{QString::number(answer)});
        return mapaWynikow;
    }
    return mapaWynikow;
}


void Rzuty::on_pushButton_rzut_clicked()
{
    QString wprowadzoe = ui->plainTextEdit_wprowadzone->toPlainText();
    QMap wynikRzucania = rezultatRzutow(wprowadzoe);
    if(wynikRzucania.value(0).at(0) == "Niepoprawny format")
    {
        QMessageBox msgBox;
        msgBox.setText("Niepoprawny format rzutu");
        msgBox.exec();
    }
    else
    {
        wyswietlRzut(wynikRzucania);
    }
}

void Rzuty::wyswietlRzut(QMap<int, QStringList> mapaRzutu)
{
    qDebug()<<mapaRzutu;
    QPlainTextEdit *wynikRzutu = new QPlainTextEdit;
    wynikRzutu->setReadOnly(1);
    QString stringZWynikiem = mapaRzutu.value(0).at(0) + "\n";
    for(int i = 1; i < mapaRzutu.size(); i++)
    {
        stringZWynikiem.append(mapaRzutu.value(i).at(1) + "\n");
    }
    wynikRzutu->setPlainText(stringZWynikiem);

    QLabel *etykietaRzutu = new QLabel;
    etykietaRzutu->setText(ui->plainTextEdit_wprowadzone->toPlainText());

    QRadioButton *wyswietlajWynik = new QRadioButton;
    wyswietlajWynik->setChecked(1);
    wyswietlajWynik->setText("Tylko wyniki");
    wyswietlajWynik->setMaximumHeight(50);

    QRadioButton *wyswietlajWynikKosciOsobno = new QRadioButton;
    wyswietlajWynikKosciOsobno->setText("Wszystkie wyniki z każdego rzutu");
    wyswietlajWynikKosciOsobno->setMaximumHeight(50);

    QRadioButton *wyswietlajWynikWynikiSuma = new QRadioButton;
    wyswietlajWynikWynikiSuma->setText("Wszystkie wyniki z każdego rzutu - suma");
    wyswietlajWynikWynikiSuma->setMaximumHeight(50);

    connect(wyswietlajWynik, &QRadioButton::clicked,
                [wyswietlajWynik, wyswietlajWynikKosciOsobno, wyswietlajWynikWynikiSuma, mapaRzutu, wynikRzutu]()
                {
                    if(wyswietlajWynik->isChecked())
                    {
                        wyswietlajWynik->setChecked(1);
                        wyswietlajWynikKosciOsobno->setChecked(0);
                        wyswietlajWynikWynikiSuma->setChecked(0);
                        QString stringZWynikiem = mapaRzutu.value(0).at(0) + "\n";
                        for(int i = 1; i < mapaRzutu.size(); i++)
                        {
                            stringZWynikiem.append(mapaRzutu.value(i).at(1) + "\n");
                        }
                        wynikRzutu->setPlainText(stringZWynikiem);
                    }
                }
            );
    connect(wyswietlajWynikKosciOsobno, &QRadioButton::clicked,
                [wyswietlajWynik, wyswietlajWynikKosciOsobno, wyswietlajWynikWynikiSuma, mapaRzutu, wynikRzutu]()
                {
                    if(wyswietlajWynikKosciOsobno->isChecked())
                    {
                        wyswietlajWynik->setChecked(0);
                        wyswietlajWynikKosciOsobno->setChecked(1);
                        wyswietlajWynikWynikiSuma->setChecked(0);
                        QString stringZWynikiem = mapaRzutu.value(0).at(0) + "\n";
                        for(int i = 1; i < mapaRzutu.size(); i++)
                        {
                            stringZWynikiem.append(mapaRzutu.value(i).at(2) + "\n");
                        }
                        wynikRzutu->setPlainText(stringZWynikiem);
                    }
                }
            );
    connect(wyswietlajWynikWynikiSuma, &QRadioButton::clicked,
                [wyswietlajWynik, wyswietlajWynikKosciOsobno, wyswietlajWynikWynikiSuma, mapaRzutu, wynikRzutu]()
                {
                    if(wyswietlajWynikWynikiSuma->isChecked())
                    {
                        wyswietlajWynik->setChecked(0);
                        wyswietlajWynikKosciOsobno->setChecked(0);
                        wyswietlajWynikWynikiSuma->setChecked(1);
                        QString stringZWynikiem = mapaRzutu.value(0).at(0) + "\n";
                        for(int i = 1; i < mapaRzutu.size(); i++)
                        {
                            stringZWynikiem.append(mapaRzutu.value(i).at(3) + "\n");
                        }
                        wynikRzutu->setPlainText(stringZWynikiem);
                    }
                }
            );

    QVBoxLayout *layoutRzut = new QVBoxLayout;
    layoutRzut->addWidget(etykietaRzutu);
    layoutRzut->addWidget(wynikRzutu);
    layoutRzut->addWidget(wyswietlajWynik);
    layoutRzut->addWidget(wyswietlajWynikKosciOsobno);
    layoutRzut->addWidget(wyswietlajWynikWynikiSuma);

    QWidget *widgetRzut = new QWidget;
    widgetRzut->setLayout(layoutRzut);


    ui->verticalLayoutWynikiRzutow->addWidget(widgetRzut);

    double testValue = te_interp("(1)", 0);
    bool test = (testValue == testValue);
}

void Rzuty::rzuc(QString rzut)
{
    QMap wynikRzucania = rezultatRzutow(rzut);
    if(wynikRzucania.value(0).at(0) == "Niepoprawny format")
    {
        QMessageBox msgBox;
        msgBox.setText("Niepoprawny format rzutu");
        msgBox.exec();
    }
    else
    {
        wyswietlRzut(wynikRzucania);
    }
}
