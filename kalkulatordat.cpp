#include "kalkulatordat.h"
#include "ui_kalkulatordat.h"
#include "QString"
#include "QFile"
#include "QFileDialog"
#include <QTextStream>
#include <QDebug>

KalkulatorDat::KalkulatorDat(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::KalkulatorDat)
{
    ui->setupUi(this);
}

KalkulatorDat::~KalkulatorDat()
{
    delete ui;
}

void KalkulatorDat::on_pushButton_2_clicked() //dodaj kolejną linie
{
    QDate data;
    doadjWiersz("", "");
}


void KalkulatorDat::on_pushButton_clicked() // usun linie
{
    int ostatniWiersz = ui->gridLayout_daty->rowCount();
    ostatniWiersz--;
    int iloscKolumn = ui->gridLayout_daty->columnCount();
    iloscKolumn--;

    for(int i = 0; i <= iloscKolumn; i++)
    {
        while(ui->gridLayout_daty->itemAtPosition(ostatniWiersz,i) == 0)
        {
            if(ostatniWiersz > 2)
            {
                ostatniWiersz--;
            }
            else
            {
                break;
            }
        }
        if(ui->gridLayout_daty->itemAtPosition(ostatniWiersz,i) != 0)
        {
            QWidget* widget = ui->gridLayout_daty->itemAtPosition(ostatniWiersz, i)->widget();
            if(widget)
            {
                ui->gridLayout_daty->removeItem(ui->gridLayout_daty->itemAtPosition(ostatniWiersz, i));
                delete ui->gridLayout_daty->itemAtPosition(ostatniWiersz, i);
                delete widget;
            }
        }
    }
}


void KalkulatorDat::on_pushButton_4_clicked() // oblicz
{
    QDate staraData;
    QDate nowaData;

    QLayoutItem *item1= ui->gridLayout_daty->itemAtPosition(1, 1);
    QDateEdit* dataS  = qobject_cast<QDateEdit*>(item1->widget());
    staraData = dataS->date();

    QLayoutItem *item2= ui->gridLayout_daty->itemAtPosition(1, 2);
    QDateEdit* dataN  = qobject_cast<QDateEdit*>(item2->widget());
    nowaData = dataN->date();


    int roznica = staraData.daysTo(nowaData);

    for (int i = 2; i < ui->gridLayout_daty->rowCount(); i++){
        QDate orginalnaData;
        if (QLayoutItem *item= ui->gridLayout_daty->itemAtPosition(i, 1)) {
            if (QDateEdit* data  = qobject_cast<QDateEdit*>(item->widget())) {
                orginalnaData = data->date();
            }
        }

        if (QLayoutItem *item= ui->gridLayout_daty->itemAtPosition(i, 2)) {
            if (QDateEdit* data  = qobject_cast<QDateEdit*>(item->widget())) {
                data->setDate(orginalnaData.addDays(roznica));
            }
        }
    }
}


void KalkulatorDat::on_pushButton_3_clicked() //zapis nowych dat
{

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               "./daty.csv",
                               tr("CSV files (*.csv)"));

    QFile file(fileName);
    if (file.open(QFile::WriteOnly|QFile::Truncate))
    {
        QTextStream stream(&file);
        QString opis = "", nowaData = "";

        for (int i = 1; i < ui->gridLayout_daty->rowCount(); i++){
            if (QLayoutItem *item= ui->gridLayout_daty->itemAtPosition(i, 0)) {

                if (QLineEdit* lineEditOpis  = qobject_cast<QLineEdit*>(item->widget())) {
                    opis = lineEditOpis->text();
                    if (QLayoutItem *item= ui->gridLayout_daty->itemAtPosition(i, 2)) {
                        if (QDateEdit* data  = qobject_cast<QDateEdit*>(item->widget())) {
                            nowaData = data->date().toString("dd.MM.yyyy");
                            stream << opis << "\t" << nowaData << "\n";
                        }
                    }
                }
            }
        }
        file.close();
    }
}

void KalkulatorDat::on_pushButton_5_clicked()
{
    otworzPlik("");
}

void KalkulatorDat::otworzPlik(QString fileName)
{
    if(fileName == "")
    {
        fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                   "./daty.csv",
                                   tr("CSV files (*.csv)"));
    }

    QFile file(fileName);
    if (file.open(QFile::ReadOnly))
    {
        while(ui->gridLayout_daty->count()>6)
        {
            QLayoutItem *item = ui->gridLayout_daty->takeAt(6);
            QWidget* widget = item->widget();
            if(widget)
                delete widget;
            delete item;
        }
        //uzupelnienie pierwszego rzedu
        QTextStream in(&file);
        QString line = in.readLine();
        QStringList lineSplited = line.split("\t");
        ui->lineEdit->setText(lineSplited.at(0));
        QDate date = QDate::fromString(lineSplited.at(1),"dd.MM.yyyy");
        ui->dateEdit->setDate(date);


        while (!in.atEnd()) {
            line = in.readLine();
            QStringList lineSplited = line.split("\t");
            QString opis = lineSplited.at(0);
            doadjWiersz(opis,lineSplited.at(1));
        }
        file.close();
    }
}

void KalkulatorDat::doadjWiersz(QString opis, QString data)
{
    QLineEdit *nazwa = new QLineEdit;
    QDateEdit *data_l = new QDateEdit;
    QDateEdit *data_p = new QDateEdit;
    data_p->setReadOnly(true);
    ui->gridLayout_daty->addWidget(nazwa);
    ui->gridLayout_daty->addWidget(data_l);
    ui->gridLayout_daty->addWidget(data_p);
    if(opis != "")
    {

        nazwa->setText(opis);
    }
    if(data != "")
    {
        QDate date = QDate::fromString(data,"dd.MM.yyyy");
        data_l->setDate(date);
    }
}
