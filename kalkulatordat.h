#ifndef KALKULATORDAT_H
#define KALKULATORDAT_H

#include <QMainWindow>

namespace Ui {
class KalkulatorDat;
}

class KalkulatorDat : public QMainWindow
{
    Q_OBJECT

public:
    explicit KalkulatorDat(QWidget *parent = nullptr);
    ~KalkulatorDat();
    void otworzPlik(QString);

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void doadjWiersz(QString, QString);

private:
    Ui::KalkulatorDat *ui;
};

#endif // KALKULATORDAT_H
