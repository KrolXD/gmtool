#ifndef KOSCI_H
#define KOSCI_H

#include <QDialog>
#include <QStringList>
#include <QRegularExpression>
#include <QMap>

namespace Ui {
class Rzuty;
}

class Rzuty : public QDialog
{
    Q_OBJECT

public:
    explicit Rzuty(QWidget *parent = nullptr);
    bool sprawdzFormat(QString);
    void rzuc(QString);
    ~Rzuty();

private slots:
    void on_pushButton_rzut_clicked();
    QStringList wykonajRzut(QString);
    QMap<int, QStringList> rezultatRzutow(QString);
    void wyswietlRzut(QMap<int, QStringList>);

private:
    Ui::Rzuty *ui;
};

#endif // KOSCI_H
