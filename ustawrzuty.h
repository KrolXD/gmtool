#ifndef USTAWRZUTY_H
#define USTAWRZUTY_H

#include <QWidget>

#include "kreator.h" //nwm czy potrzebne

#include<QLabel>
#include<QList>
#include<QComboBox>
#include<QLineEdit>
#include<QPlainTextEdit>
#include<QSpinBox>
#include<QCheckBox>
#include<QMultiMap>

namespace Ui {
class UstawRzuty;
}

class UstawRzuty : public QWidget
{
    Q_OBJECT

public:
    explicit UstawRzuty(QWidget *parent = nullptr);
    ~UstawRzuty();
    void ustawTable();
    void doajWiersze(int, int, QString, int, QLineEdit*, QLineEdit*, QPushButton*, QSpinBox*, QString);

signals:
    bool updateRzuty(QMultiMap <int, QMap<int, QString>>);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void ustawTablePodstawowa();

private:
    Ui::UstawRzuty *ui;

    struct wierszZRzutem
    {
        QString nazwaSegmentu;
        int nrSeg;
        int wiersz;
        int typWiersza;
        QLineEdit *etykieta;
        QLineEdit *poleTekstowe;
        QPushButton *rollPushButton;
        QSpinBox *poleNumeryczne;
        QCheckBox *checkBox;
        QLineEdit *komendaRzutu;
    };
    QList <wierszZRzutem>listaWierszy;

    struct wierszZWartoscia
    {
        QString nazwaSegmentu;
        int nrSeg;
        int wiersz;
        QLineEdit *etykieta;
        QLineEdit *poleTekstowe;
        QSpinBox *poleNumeryczne;
        QCheckBox *checkBox;
    };
    QList <wierszZWartoscia>listaWierszyZWartoscia;

    QStringList opcjeWarotsci;

    bool trybZaawansowany = 0;
};

#endif // USTAWRZUTY_H
