#include "ustawrzuty.h"
#include "ui_ustawrzuty.h"
#include "rzuty.h"
#include <QDebug>
#include <QLabel>
#include <QLineEdit>
#include <QtDebug>
#include <QMessageBox>
#include <QMultiMap>
#include <QMap>
#include <QTreeWidgetItem>
#include <QRegExp>

UstawRzuty::UstawRzuty(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UstawRzuty)
{
    ui->setupUi(this);
}

UstawRzuty::~UstawRzuty()
{
    delete ui;
}

void UstawRzuty::ustawTable()
{
    ustawTablePodstawowa();
    ui->widget_rzutyZaawansowane->setVisible(0);
    ui->treeWidget->setVisible(0);
}

void UstawRzuty::ustawTablePodstawowa()
{
    QString poprzedniSeg = "";
    int topLvlItem = -1;
    for(auto wiersz : listaWierszyZWartoscia)
    {
        QString opcja = "[s:" + QString::number(wiersz.nrSeg) + " w:" + QString::number(wiersz.wiersz) + "]";
        opcjeWarotsci.append(opcja);

        if(poprzedniSeg != wiersz.nazwaSegmentu)
        {
            auto *item = new QTreeWidgetItem;
            item->setText(0, wiersz.nazwaSegmentu);
            ui->treeWidget->addTopLevelItem(item);
            topLvlItem++;
        }
        auto *item = new QTreeWidgetItem;
        item->setText(0, opcja);
        item->setText(1, wiersz.etykieta->text());
        ui->treeWidget->topLevelItem(topLvlItem)->addChild(item);
    }

    poprzedniSeg = "";

    for(auto wiersz : listaWierszy)
    {

        if(poprzedniSeg != wiersz.nazwaSegmentu)
        {
            qDebug()<<"1";
            auto *tytulSeg = new QLabel;
            tytulSeg->setText(wiersz.nazwaSegmentu);
            tytulSeg->setMaximumHeight(30);
            tytulSeg->setAlignment(Qt::AlignHCenter);
            ui->gridLayout->addWidget(tytulSeg, ui->gridLayout->rowCount(), 0, 1, 4);
        }

        auto *etykieta = new QLabel;
        etykieta->setText(wiersz.etykieta->text());

        auto *numer = new QLabel;
        numer->setText(QString::number(wiersz.wiersz));

        qDebug()<<"2";
        int row =  ui->gridLayout->rowCount();
        ui->gridLayout->addWidget(numer, row, 0);
        ui->gridLayout->addWidget(etykieta, row, 1);
        ui->gridLayout->addWidget(wiersz.komendaRzutu, row, 2);
        ui->gridLayout->addWidget(wiersz.checkBox, row, 3);

        poprzedniSeg = wiersz.nazwaSegmentu;
    }
}

void UstawRzuty::doajWiersze(int typWiersza, int nrSeg, QString nazwaSegmentu, int wiersz, QLineEdit *etykieta, QLineEdit *poleTekstowe, QPushButton *rollPushButton, QSpinBox *poleNumeryczne, QString rzut)
{
    if(typWiersza == 3)
    {
        auto *zaznaczenie = new QCheckBox;
        auto *komenda = new QLineEdit;
        komenda->setClearButtonEnabled(1);
        komenda->setText(rzut);

        wierszZRzutem wierszZRzutem;
        wierszZRzutem.nrSeg = nrSeg;
        wierszZRzutem.nazwaSegmentu = nazwaSegmentu;
        wierszZRzutem.wiersz = wiersz;
        wierszZRzutem.etykieta = etykieta;
        wierszZRzutem.poleTekstowe = poleTekstowe;
        wierszZRzutem.rollPushButton = rollPushButton;
        wierszZRzutem.poleNumeryczne = poleNumeryczne;
        wierszZRzutem.komendaRzutu = komenda;
        wierszZRzutem.checkBox = zaznaczenie;
        listaWierszy.append(wierszZRzutem);
    }
    else
    {
        auto *zaznaczenie = new QCheckBox;
        wierszZWartoscia  wierszZWartoscia;
        wierszZWartoscia.wiersz = wiersz;
        wierszZWartoscia.etykieta = etykieta;
        wierszZWartoscia.nrSeg = nrSeg;
        wierszZWartoscia.nazwaSegmentu = nazwaSegmentu;
        wierszZWartoscia.checkBox = zaznaczenie;
        listaWierszyZWartoscia.append(wierszZWartoscia);
    }
}

void UstawRzuty::on_pushButton_clicked()
{
    for(auto wiersz : listaWierszy)
    {
        if(wiersz.checkBox->isChecked())
        {
            wiersz.komendaRzutu->setText(ui->lineEdit->text());
        }
    }
}



void UstawRzuty::on_pushButton_3_clicked()
{
    bool blad = false;

    for(auto wiersz : listaWierszy)
    {
        auto sprRzut = new Rzuty;
        QString sprawdz = wiersz.komendaRzutu->text().replace(QRegExp("(\\[s:(\\d)+ w:(\\d)+\\])"), "1"); //zwalidować czy [] jest poprawne

        if(!sprRzut->sprawdzFormat(sprawdz))
        {
            wiersz.komendaRzutu->setStyleSheet("QLineEdit{border: 1px solid red;}");
            blad = true;
        }
        else
        {
            wiersz.komendaRzutu->setStyleSheet("QLineEdit{border: 1px solid black;}");
        }
    }
    if(blad)
    {
        QMessageBox msgBox;
        msgBox.setText("Popraw zaznaczone pola.");
        msgBox.exec();
    }
    else
    {
        QMultiMap <int, QMap<int, QString>> mapdDoPrzekazania;
        for(auto wiersz : listaWierszy)
        {
            QMap <int, QString> mapaWiersza;
            mapaWiersza.insert(0, wiersz.komendaRzutu->text());
            mapaWiersza.insert(1, QString::number(wiersz.wiersz));
            mapdDoPrzekazania.insert(wiersz.nrSeg, mapaWiersza);
        }
        emit this->updateRzuty(mapdDoPrzekazania);
        this->close();
        delete this;
    }
}


void UstawRzuty::on_pushButton_2_clicked() //zmienia widok kreatorza rzutów
{
    if(trybZaawansowany)
    {
        ui->treeWidget->setVisible(0);
        ui->pushButton_2->setText("Ukryj pola numeryczne");
        trybZaawansowany = false;
    }
    else
    {
        ui->treeWidget->setVisible(1);
        ui->pushButton_2->setText("Pokaż pola numeryczne");
        trybZaawansowany = true;
    }
}

