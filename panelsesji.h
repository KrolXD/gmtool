#ifndef PANELSESJI_H
#define PANELSESJI_H

#include <QMainWindow>
#include <QMap>
#include <QPushButton>
#include <QVBoxLayout>

#include <QLineEdit>>
#include <QPlainTextEdit>
#include <QStringList>


namespace Ui {
class PanelSesji;
}

class PanelSesji : public QMainWindow
{
    Q_OBJECT

public:
    explicit PanelSesji(QWidget *parent = nullptr);
    ~PanelSesji();

private slots:
    void on_tabWidget_tabCloseRequested(int index);

    void on_pushButton_clicked();

    void dodajElement();

    void on_actionOtw_rz_triggered();

    void on_actionZapisz_triggered();

    void wyczyscPrzedOtwarciem();

signals:
    QStringList doOtwarcia(QStringList);

private:
    Ui::PanelSesji *ui;

    QMap<QPushButton *,QVBoxLayout *> mapaWidokowKart;

    struct ElementZakladki
    {
        QLineEdit * etykieta;
        QPlainTextEdit * notatki;
        QStringList polaczenia;
    };

    struct Zakladka
    {
        QWidget * tabWidget;
        QString tytul;
        QList<ElementZakladki> listaZawartosciElementow;
    };

    QMap<int, Zakladka> mapaZakladek;

    int zakladkaID = 0;
};

#endif // PANELSESJI_H
