/*
  This file is part of KDDockWidgets.
  SPDX-FileCopyrightText: 2019-2022 Klarälvdalens Datakonsult AB, a KDAB Group company <info@kdab.com>
  Author: Sérgio Martins <sergio.martins@kdab.com>
  SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
  Contact KDAB at <info@kdab.com> for commercial licensing options.
*/
#include "gmtoolmainwindow.h"

#include <kddockwidgets/Config.h>
#include <kddockwidgets/LayoutSaver.h>

#include <QMenu>
#include <QMenuBar>
#include <QEvent>
#include <QDebug>
#include <QString>
#include <QTextEdit>
#include <QRandomGenerator>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QApplication>

#include "kalkulatordat.h"
#include "kreator.h"
#include "rzuty.h"
#include "kartypostaci.h"
#include "panelsesji.h"


GmToolMainWindow::GmToolMainWindow(const QString &uniqueName, KDDockWidgets::MainWindowOptions options,
                           bool restoreIsRelative,
                           const QString &affinityName, QWidget *parent)
    : MainWindow(uniqueName, options, parent)
    , m_restoreIsRelative(restoreIsRelative)
{
    auto menubar = menuBar();
    auto fileMenu = new QMenu(QStringLiteral("File"), this);
    auto menuNarzedzia = new QMenu(QStringLiteral("Narzędzia"), this);
    auto miscMenu = new QMenu(QStringLiteral("Misc"), this);
    auto menuWidok = new QMenu(QStringLiteral("Widok"), this);

    rzutyDoKart = new Rzuty();
    dockRzutyDoKart = new KDDockWidgets::DockWidget(QStringLiteral("Rzuty #%1").arg(ileRzuty));
    dockRzutyDoKart->setWidget(rzutyDoKart);
    ileRzuty++;
    m_dockwidgets<<dockRzutyDoKart;
    QAction *rzutyDoKartAction = menuWidok->addAction(QStringLiteral("Pokaż głowne okno rzutów"));
    connect(rzutyDoKartAction, &QAction::triggered, this, [this, &dockRzutyDoKart = dockRzutyDoKart, &rzutyDoKart = rzutyDoKart] {
        if(dockRzutyDoKart->isHidden())
        {
            dockRzutyDoKart->show();
            rzutyDoKart->show();
        }
    });

    menubar->addMenu(fileMenu);
    menubar->addMenu(miscMenu);
    menubar->addMenu(menuNarzedzia);
    menubar->addMenu(menuWidok);

    QAction *kalkulatorDatAction = menuNarzedzia->addAction(QStringLiteral("Kalkulator dat"));
    connect(kalkulatorDatAction, &QAction::triggered, this, [this] {
        auto dock = new KDDockWidgets::DockWidget(QStringLiteral("Kalkulator #%1").arg(ileKalkulator));
        ileKalkulator++;
        auto kalkulator = new KalkulatorDat();
        dock->setWidget(kalkulator);
        dock->show();
        m_dockwidgets<<dock;
    });
    QAction *rzutyAction = menuNarzedzia->addAction(QStringLiteral("Rzuty"));
    connect(rzutyAction, &QAction::triggered, this, [this] {
        auto dock = new KDDockWidgets::DockWidget(QStringLiteral("Rzuty #%1").arg(ileRzuty));
        ileRzuty++;
        auto rzuty = new Rzuty();
        dock->setWidget(rzuty);
        dock->show();
        m_dockwidgets<<dock;
    });
    QAction *kreatorAction = menuNarzedzia->addAction(QStringLiteral("Kreator kart postaci"));
    connect(kreatorAction, &QAction::triggered, this, [this] {
        auto dock = new KDDockWidgets::DockWidget(QStringLiteral("Kreator kart postaci #%1").arg(ileKreator));
        ileKreator++;
        auto kreator  = new Kreator();
        dock->setWidget(kreator);
        dock->show();
        m_dockwidgets<<dock;
    });
    QAction *kartyAction = menuNarzedzia->addAction(QStringLiteral("Karty postaci"));
    connect(kartyAction, &QAction::triggered, this, [this, &rzutyDoKart = rzutyDoKart] {
        auto dock = new KDDockWidgets::DockWidget(QStringLiteral("Karty postaci #%1").arg(ileKarty));
        ileKarty++;
        auto kartypostaci  = new KartyPostaci();
        dock->setWidget(kartypostaci);
        addDockWidgetAsTab(dock);

        kartypostaci->getRzuty(rzutyDoKart);
        dock->show();
        m_dockwidgets<<dock;
    });

    QAction *panelSesji = menuNarzedzia->addAction(QStringLiteral("Panel sesji"));
    connect(panelSesji, &QAction::triggered, this, [this] {
        auto dock = new KDDockWidgets::DockWidget(QStringLiteral("Panel sesji #%1").arg(ileSesji));
        ileSesji++;
        auto panel  = new PanelSesji();
        connect(panel, SIGNAL(doOtwarcia(QStringList)), this, SLOT(otworzZListy(QStringList)));
        dock->setWidget(panel);
        dock->show();
        m_dockwidgets<<dock;
    });

    auto saveLayoutAction = fileMenu->addAction(QStringLiteral("Save Layout"));
    connect(saveLayoutAction, &QAction::triggered, this, [] {
        KDDockWidgets::LayoutSaver saver;
        const bool result = saver.saveToFile(QStringLiteral("mylayout.json"));
        qDebug() << "Saving layout to disk. Result=" << result;
    });

    auto restoreLayoutAction = fileMenu->addAction(QStringLiteral("Restore Layout"));
    connect(restoreLayoutAction, &QAction::triggered, this, [this] {
        KDDockWidgets::RestoreOptions options = KDDockWidgets::RestoreOption_None;
        if (m_restoreIsRelative)
            options |= KDDockWidgets::RestoreOption_RelativeToMainWindow;

        restoreWidgets();
        KDDockWidgets::LayoutSaver saver(options);
        saver.restoreFromFile(QStringLiteral("mylayout.json"));
    });

    auto closeAllAction = fileMenu->addAction(QStringLiteral("Close All"));
    connect(closeAllAction, &QAction::triggered, this, [this] {
        for (auto dw : qAsConst(m_dockwidgets))
        {
            dw->close();
            delete dw;
        }
        m_dockwidgets.clear();
    });

    auto layoutEqually = fileMenu->addAction(QStringLiteral("Layout Equally"));
    connect(layoutEqually, &QAction::triggered, this, &MainWindow::layoutEqually);

    auto quitAction = fileMenu->addAction(QStringLiteral("Quit"));
    connect(quitAction, &QAction::triggered, qApp, &QApplication::quit);

    QAction *toggleDropIndicatorSupport = miscMenu->addAction(QStringLiteral("Toggle Drop Indicator Support"));
    toggleDropIndicatorSupport->setCheckable(true);
    toggleDropIndicatorSupport->setChecked(true);
    connect(toggleDropIndicatorSupport, &QAction::toggled, this, [](bool checked) {
        KDDockWidgets::Config::self().setDropIndicatorsInhibited(!checked);
    });

    setAffinities({ affinityName });
}

GmToolMainWindow::~GmToolMainWindow()
{
    qDeleteAll(m_dockwidgets);
}

void GmToolMainWindow::restoreWidgets()
{
    for (auto dw : qAsConst(m_dockwidgets))
    {
        dw->close();
        delete dw;
    }
    m_dockwidgets.clear();
    QString jsonFilename = QStringLiteral("mylayout.json");
    QFile f(jsonFilename);
    if (!f.open(QIODevice::ReadOnly))
    {
        qWarning() << Q_FUNC_INFO << "Failed to open" << jsonFilename << f.errorString();
    }
    else
    {
        QString data = f.readAll();
        QJsonDocument d = QJsonDocument::fromJson(data.toUtf8());
        QJsonObject o = d.object();
        QJsonArray a = o.value("allDockWidgets").toArray();
        QJsonObject o2;
        ileKalkulator = ileKreator = ileKarty = ileRzuty = 0;
        for(int i = 0; i < a.size(); i++)
        {
            o2 = a[i].toObject();
            QString nazwa = o2.value("uniqueName").toString();
            if(nazwa.contains("Kalkulator"))
            {
                auto dock = new KDDockWidgets::DockWidget(nazwa);
                ileKalkulator++;
                auto kalkulator = new KalkulatorDat();
                dock->setWidget(kalkulator);
                dock->show();
                m_dockwidgets<<dock;
            }
            else if(nazwa.contains("Rzuty"))
            {
                auto dock = new KDDockWidgets::DockWidget(nazwa);
                ileKreator++;
                auto rzuty = new Rzuty();
                dock->setWidget(rzuty);
                dock->show();
                m_dockwidgets<<dock;
            }
            else if(nazwa.contains("Kreator"))
            {
                auto dock = new KDDockWidgets::DockWidget(nazwa);
                ileRzuty++;
                auto kreator = new Kreator();
                dock->setWidget(kreator);
                dock->show();
                m_dockwidgets<<dock;
            }
            else if(nazwa.contains("Karty"))
            {
                auto dock = new KDDockWidgets::DockWidget(nazwa);
                ileRzuty++;
                auto karta = new KartyPostaci();
                dock->setWidget(karta);
                dock->show();
                m_dockwidgets<<dock;
            }
            else if(nazwa.contains("Panel"))
            {
                auto dock = new KDDockWidgets::DockWidget(nazwa);
                ileRzuty++;
                auto panel = new PanelSesji();
                dock->setWidget(panel);
                dock->show();
                m_dockwidgets<<dock;
            }
        }
    }
    qDebug()<<m_dockwidgets;
    f.close();
}

void GmToolMainWindow::otworzZListy(QStringList lista)
{
    for(auto element : lista)
    {
        if(element.contains(".csv"))
        {
            otworzDaty(element);
        }
        else if(element.contains(".xml"))
        {
            otworzKarte(element);
        }
    }
}

void GmToolMainWindow::otworzKarte(QString filename)
{
    auto dock = new KDDockWidgets::DockWidget(QStringLiteral("Karty postaci #%1").arg(ileKarty));
    ileKarty++;
    auto kartypostaci  = new KartyPostaci();
    dock->setWidget(kartypostaci);
    addDockWidgetAsTab(dock);

    kartypostaci->getRzuty(rzutyDoKart);
    dock->show();
    m_dockwidgets<<dock;
    kartypostaci->otworz(filename); //zostaje
}

void GmToolMainWindow::otworzDaty(QString filename)
{
    auto dock = new KDDockWidgets::DockWidget(QStringLiteral("Kalkulator #%1").arg(ileKalkulator));
    ileKalkulator++;
    auto kalkulator = new KalkulatorDat();
    kalkulator->otworzPlik(filename);
    dock->setWidget(kalkulator);
    dock->show();
    m_dockwidgets<<dock;
}
