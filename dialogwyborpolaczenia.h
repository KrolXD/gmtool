#ifndef DIALOGWYBORPOLACZENIA_H
#define DIALOGWYBORPOLACZENIA_H

#include <QDialog>

namespace Ui {
class DialogWyborPolaczenia;
}

class DialogWyborPolaczenia : public QDialog
{
    Q_OBJECT

public:
    explicit DialogWyborPolaczenia(QWidget *parent = nullptr);
    ~DialogWyborPolaczenia();

private:
    Ui::DialogWyborPolaczenia *ui;
};

#endif // DIALOGWYBORPOLACZENIA_H
