#ifndef KREATOR_H
#define KREATOR_H


#include <QMainWindow>
#include <QGridLayout>
#include <QMap>
#include <QComboBox>
#include <QCheckBox>
#include <QString>
#include <QPushButton>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QList>
#include <QSpinBox>
#include <QCheckBox>



namespace Ui {
class Kreator;
}

class Kreator : public QMainWindow
{
    Q_OBJECT

public:
    explicit Kreator(QWidget *parent = nullptr);
    ~Kreator();
    //struct DaneZWiersza
    //{
    //    int segment;
    //    int wiersz;
    //    int typWiersza;
    //    QLineEdit *etykieta;
    //    QComboBox *comboBox;
    //    QLineEdit *poleTekstowe;
    //    QPlainTextEdit *textBox;
    //    QPushButton *rollPushButton;
    //    QSpinBox *poleNumeryczne;
    //    QCheckBox *checkBox;
    //};

private slots:
    void dodajPole();
    void dodajPoleTekstoweLinia(QGridLayout*, int, QString, QString, bool); //typ 0
    void dodajPoleTekstoweBox(QGridLayout*, int, QString, QString, bool); //typ 1
    void dodajPoleNumeryczne(QGridLayout*, int, QString, QString, bool); //typ 2
    void dodajPoleListyWyboru(QGridLayout*, int, QString, QString, bool); //typ 3
    void dodajPrzycisk(QGridLayout*, int, QString, QString, bool); //typ 4
    void czyszczeniePrzedWczytaniem();
    void usunSegment();

    void on_actionSegment_triggered();

    void on_actionZapisz_triggered();

    void on_actionOtw_rz_triggered();

    void on_actionRzuty_triggered();

private:
    Ui::Kreator *ui;
    int ileSeg = 0;
    //mapy segmentu
    QMap<int, QGridLayout *> mapaSegmentow;
    QMap<int, QCheckBox *> checkBoxMap;
    QMap<int, QComboBox *> comboBoxMapWybor;
    QMap<int, QLineEdit *> titlesMap;

    struct DaneZWiersza
    {
        int segment;
        int wiersz;
        int typWiersza;
        QLineEdit *etykieta;
        QComboBox *comboBox;
        QLineEdit *poleTekstowe;
        QPlainTextEdit *textBox;
        QPushButton *rollPushButton;
        QSpinBox *poleNumeryczne;
        QCheckBox *checkBox;
        QString rzut;
    };


    QList<DaneZWiersza> listaWierszy;
};

#endif // KREATOR_H
