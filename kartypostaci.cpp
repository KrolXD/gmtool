#include "kartypostaci.h"
#include "ui_kartypostaci.h"
// import()
#include <QFileDialog>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
// dodajSegment()
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>

#include "rzuty.h"


#include<QDebug>

KartyPostaci::KartyPostaci(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::KartyPostaci)
{
    ui->setupUi(this);
}

KartyPostaci::~KartyPostaci()
{
    delete ui;
}

void KartyPostaci::import(QString filename)
{
    if(filename == "")
    {
        filename = QFileDialog::getOpenFileName(this, tr("Save Xml"), "./karta.xml", tr("XML files (*.xml)"));
    }
    QFile file(filename);
    if(file.open(QIODevice::ReadOnly))
    {
        QString etykietaValue = "";
        QXmlStreamReader Rxml;
        bool isChecked = false;
        bool czyCheckBoxJest = false;

        Rxml.setDevice(&file);

        while(!Rxml.atEnd())
        {
            if(Rxml.isStartElement())
            {
                if(Rxml.name().toString() == "etykieta")
                {
                    etykietaValue = Rxml.readElementText();
                }
                else if(Rxml.name().toString() == "checkBox")
                {
                    isChecked =Rxml.attributes().value("isChecked").toString().toInt();
                    czyCheckBoxJest = Rxml.attributes().value("czyCheckBox").toString().toInt();
                }
                else if(Rxml.name().toString() == "poleTekstowe") //jeśli linia tekstu
                {
                    QGridLayout *segment = mapaSegmentow.value(ileSeg-1);
                    dodajPoleTekstoweLinia(segment, ileSeg-1, etykietaValue, Rxml.readElementText(), czyCheckBoxJest, isChecked);
                }
                else if(Rxml.name().toString() == "textBox") //jeśli tekst box
                {
                    QGridLayout *segment = mapaSegmentow.value(ileSeg-1);
                    dodajPoleTekstoweBox(segment, ileSeg-1, etykietaValue, Rxml.readElementText(), czyCheckBoxJest, isChecked);
                }
                else if(Rxml.name().toString() == "poleNum") //jeśli pole numeryczne
                {
                    QString kod = Rxml.attributes().value("kod").toString();
                    Rxml.readNext();
                    Rxml.readNext();
                    if(Rxml.name().toString() == "poleNumeryczne")
                    {
                        QGridLayout *segment = mapaSegmentow.value(ileSeg-1);
                        dodajPoleNumeryczne(segment, ileSeg-1, etykietaValue, Rxml.readElementText(), czyCheckBoxJest, isChecked, kod);
                    }
                }
                else if(Rxml.name().toString() == "przycisk") //jeśli pole numeryczne z przyciskiem
                {
                    QGridLayout *segment = mapaSegmentow.value(ileSeg-1);
                    dodajPrzycisk(segment, ileSeg-1, etykietaValue, Rxml.readElementText(), czyCheckBoxJest, isChecked);
                }
                else if(Rxml.name().toString() == "comboBox")//jeśli pole z listą
                {
                    QGridLayout *segment = mapaSegmentow.value(ileSeg-1);
                    dodajPoleListyWyboru(segment, ileSeg-1, etykietaValue, Rxml.readElementText(), czyCheckBoxJest, isChecked);
                }
                else if(Rxml.name().toString() == "segment")
                {
                    dodajSegment(Rxml.attributes().value("tytul").toString());
                }
            }
            Rxml.readNext();
        }
    }
    file.close();
}

void KartyPostaci::dodajSegment(QString tytul)//tworzy segment
{
    auto *segment = new QGridLayout;

    mapaSegmentow.insert(ileSeg, segment);

    auto *tytulSeg = new QLabel;
    tytulSeg->setText(tytul);
    tytulSeg->setSizePolicy (QSizePolicy::Minimum , QSizePolicy::Fixed);
    tytulSeg->setAlignment(Qt::AlignHCenter);

    segment->addWidget(tytulSeg,0,0,1,5);
    ui->verticalLayoutSeg->addLayout(segment);
    mapaTytulow.insert(ileSeg, tytul);
    ileSeg++;
}


void KartyPostaci::dodajPoleTekstoweLinia(QGridLayout *segment, int ktorySeg, QString etykietaValue, QString poleTekstoweValue, bool czyCheckBoxJest, bool isChecked)
{
    auto *etykieta = new QLabel;
    etykieta->setMaximumWidth(150);
    etykieta->setText(etykietaValue);

    auto *poleTekstowe = new QLineEdit;
    poleTekstowe->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    poleTekstowe->setText(poleTekstoweValue);

    int row = segment->rowCount();

    DaneZWiersza daneZWiersza;
    daneZWiersza.typWiersza = 0;
    daneZWiersza.segment = ktorySeg;
    daneZWiersza.wiersz = row;
    daneZWiersza.etykieta = etykieta;
    daneZWiersza.poleTekstowe = poleTekstowe;

    if(czyCheckBoxJest)
    {
        auto *checkBox = new QCheckBox;
        checkBox->setChecked(isChecked);
        daneZWiersza.checkBox = checkBox;
        checkBox->setMaximumWidth(25);

        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleTekstowe, row, 1, 1, 2);
        segment->addWidget(checkBox, row, 3);
    }
    else
    {
        daneZWiersza.checkBox = NULL;
        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleTekstowe, row, 1, 1, 3);
    }

    listaWierszy.push_back(daneZWiersza);
}

void KartyPostaci::dodajPoleTekstoweBox(QGridLayout *segment, int ktorySeg, QString etykietaValue, QString poleTekstoweValue, bool czyCheckBoxJest, bool isChecked)
{
    auto *etykieta = new QLabel;
    etykieta->setMaximumWidth(150);
    etykieta->setText(etykietaValue);

    auto *poleTekstowe = new QPlainTextEdit;
    poleTekstowe->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    poleTekstowe->insertPlainText(poleTekstoweValue);

    int row = segment->rowCount();

    DaneZWiersza daneZWiersza;
    daneZWiersza.typWiersza = 1;
    daneZWiersza.segment = ktorySeg;
    daneZWiersza.wiersz = row;
    daneZWiersza.etykieta = etykieta;
    daneZWiersza.textBox = poleTekstowe;

    if(czyCheckBoxJest)
    {
        auto *checkBox = new QCheckBox;
        checkBox->setChecked(isChecked);
        daneZWiersza.checkBox = checkBox;
        checkBox->setMaximumWidth(25);

        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleTekstowe, row, 1, 1, 2);
        segment->addWidget(checkBox, row, 3);
    }
    else
    {
        daneZWiersza.checkBox = NULL;
        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleTekstowe, row, 1, 1, 3);
    }

    listaWierszy.push_back(daneZWiersza);
}

void KartyPostaci::dodajPoleNumeryczne(QGridLayout *segment, int ktorySeg, QString etykietaValue, QString poleNumeryczneValue, bool czyCheckBoxJest, bool isChecked, QString kod)
{
    auto *etykieta = new QLabel;
    etykieta->setMaximumWidth(150);
    etykieta->setText(etykietaValue);

    auto *poleNumeryczne = new QSpinBox;
    poleNumeryczne->setMaximum(2147483647);
    poleNumeryczne->setMinimum(-2147483648);
    if(poleNumeryczneValue != "")
    {
        poleNumeryczne->setValue(poleNumeryczneValue.toInt());
    }

    int row = segment->rowCount();

    DaneZWiersza daneZWiersza;
    daneZWiersza.typWiersza = 2;
    daneZWiersza.segment = ktorySeg;
    daneZWiersza.wiersz = row;
    daneZWiersza.etykieta = etykieta;
    daneZWiersza.poleNumeryczne = poleNumeryczne;

    segment->addWidget(etykieta, row, 0, 1, 1);
    segment->addWidget(poleNumeryczne, row, 1, 1, 2);

    if(czyCheckBoxJest)
    {
        auto *checkBox = new QCheckBox;
        checkBox->setChecked(isChecked);
        daneZWiersza.checkBox = checkBox;
        checkBox->setMaximumWidth(25);
        segment->addWidget(checkBox, row, 3);
    }
    else
    {
        daneZWiersza.checkBox = NULL;
    }

    listaWierszy.push_back(daneZWiersza);
    mapaPolWartosci.insert(kod, poleNumeryczne);
}

void KartyPostaci::dodajPrzycisk(QGridLayout *segment, int ktorySeg, QString etykietaValue, QString rzut, bool czyCheckBoxJest, bool isChecked)
{
    auto *etykieta = new QLabel;
    etykieta->setMaximumWidth(150);
    etykieta->setText(etykietaValue);

    auto *rollButton = new QPushButton;
    rollButton->setText("roll");
    rollButton->setMaximumWidth(25);

    connect(rollButton, &QPushButton::pressed,
            [rzut, &rzutyPrzypisaneDoKarty = rzutyPrzypisaneDoKarty, &mapaPolWartosci = mapaPolWartosci]()
                {
                    QString sRzut = rzut;
                    QList klucze  = mapaPolWartosci.keys();
                    for(auto klucz : klucze)
                    {
                        sRzut = sRzut.replace(klucz, "(" + QString::number(mapaPolWartosci.value(klucz)->value()) + ")") ;
                    }
                    rzutyPrzypisaneDoKarty->rzuc(sRzut);
                });

    int row = segment->rowCount();

    DaneZWiersza daneZWiersza;
    daneZWiersza.typWiersza = 3;
    daneZWiersza.segment = ktorySeg;
    daneZWiersza.wiersz = row;
    daneZWiersza.etykieta = etykieta;
    daneZWiersza.rzut = rzut;

    segment->addWidget(etykieta, row, 0, 1, 1);
    segment->addWidget(rollButton, row, 2);

    if(czyCheckBoxJest)
    {
        auto *checkBox = new QCheckBox;
        checkBox->setChecked(isChecked);
        checkBox->setMaximumWidth(25);
        daneZWiersza.checkBox = checkBox;
        segment->addWidget(checkBox, row, 3);

    }
    else
    {
        daneZWiersza.checkBox = NULL;
    }

    listaWierszy.push_back(daneZWiersza);
}

void KartyPostaci::dodajPoleListyWyboru(QGridLayout *segment, int ktorySeg, QString etykietaValue, QString poleListyWyboruValue, bool czyCheckBoxJest, bool isChecked)
{
    auto *etykieta = new QLabel;
    etykieta->setMaximumWidth(150);
    etykieta->setText(etykietaValue);

    auto *poleListyWyboru = new QComboBox;
    if(poleListyWyboruValue != "")
    {
        QStringList itemsList = poleListyWyboruValue.split("\n");
        poleListyWyboru->addItems(itemsList);
    }

    int row = segment->rowCount();

    DaneZWiersza daneZWiersza;
    daneZWiersza.typWiersza = 4;
    daneZWiersza.segment = ktorySeg;
    daneZWiersza.wiersz = row;
    daneZWiersza.etykieta = etykieta;
    daneZWiersza.comboBox = poleListyWyboru;

    if(czyCheckBoxJest)
    {
        auto *checkBox = new QCheckBox;
        checkBox->setChecked(isChecked);
        daneZWiersza.checkBox = checkBox;
        checkBox->setMaximumWidth(25);

        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleListyWyboru, row, 1, 1, 2);
        segment->addWidget(checkBox, row, 3);
    }
    else
    {
        daneZWiersza.checkBox = NULL;
        segment->addWidget(etykieta, row, 0, 1, 1);
        segment->addWidget(poleListyWyboru, row, 1, 1, 3);
    }

    listaWierszy.push_back(daneZWiersza);
}

void KartyPostaci::getRzuty(Rzuty *przypisaneRzuty)
{
    rzutyPrzypisaneDoKarty = przypisaneRzuty;
}

void KartyPostaci::on_actionZapisz_triggered()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Xml"), "./karta.xml",
                                                    tr("XML files (*.xml)"));
    QFile file(filename);
    if(file.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter xmlWriter(&file);
        xmlWriter.setAutoFormatting(true);
        xmlWriter.writeStartDocument();
        xmlWriter.writeStartElement("karta");
        for(int i = 0; i < ileSeg; i++)
        {
            if(mapaTytulow.value(i) != NULL)
            {
                xmlWriter.writeStartElement("segment");
                xmlWriter.writeAttribute("tytul",mapaTytulow.value(i));
                qDebug()<<mapaTytulow.value(i);
                for(auto wiersz : listaWierszy)
                {
                    if(wiersz.segment == i)
                    {
                        xmlWriter.writeStartElement("wiersz");
                        int typWiersza = wiersz.typWiersza;
                        QLabel *etykieta = wiersz.etykieta;

                        xmlWriter.writeAttribute("typ", QString::number(typWiersza));
                        xmlWriter.writeTextElement("etykieta", etykieta->text());
                        if(wiersz.checkBox != NULL)
                        {
                            bool checkBoxChecked = wiersz.checkBox->isChecked();
                            xmlWriter.writeStartElement("checkBox");
                            xmlWriter.writeAttribute("czyCheckBox", QString::number(1));
                            xmlWriter.writeAttribute("isChecked", QString::number(checkBoxChecked));
                            xmlWriter.writeEndElement();
                        }
                        else
                        {
                            xmlWriter.writeStartElement("checkBox");
                            xmlWriter.writeAttribute("czyCheckBox", QString::number(0));
                            xmlWriter.writeAttribute("isChecked", QString::number(0));
                            xmlWriter.writeEndElement();
                        }

                        switch(typWiersza)
                        {
                        case 0:
                            xmlWriter.writeTextElement("poleTekstowe", wiersz.poleTekstowe->text());
                        break;

                        case 1:
                            xmlWriter.writeTextElement("textBox", wiersz.textBox->toPlainText());
                        break;

                        case 2:
                            xmlWriter.writeStartElement("poleNum");
                            xmlWriter.writeAttribute("kod", "[s:" + QString::number(wiersz.segment) + " w:" + QString::number(wiersz.wiersz+2) + "]");
                            xmlWriter.writeTextElement("poleNumeryczne", QString::number(wiersz.poleNumeryczne->value()));
                            xmlWriter.writeEndElement();
                        break;

                        case 3:
                            xmlWriter.writeTextElement("przycisk", wiersz.rzut);
                        break;

                        case 4:
                            QComboBox *comboBox = wiersz.comboBox;
                            QString itemsText = "";
                            for(int x = 0; x<comboBox->count(); x++)
                            {
                                itemsText += comboBox->itemText(x) + "\n";
                            }
                            xmlWriter.writeTextElement("comboBox", itemsText);
                        break;
                        }

                        xmlWriter.writeEndElement();
                    }
                }
                xmlWriter.writeEndElement();
            }
        }
        xmlWriter.writeEndElement();
        xmlWriter.writeEndDocument();
        file.close();
    }
}

void KartyPostaci::wyczysc()
{
    if(ileSeg != 0)
    {
        for (auto mapa = mapaSegmentow.begin(); mapa != mapaSegmentow.end(); ++mapa)
        {
            QGridLayout *segment = mapa.value();
            while(segment->count() > 0)
            {
                QLayoutItem *item = segment->takeAt(0);
                QWidget* widget = item->widget();
                if(widget)
                    delete widget;
                delete item;
            }
            delete segment;
        }

        //zerowanie zmiennych
        mapaSegmentow.clear();
        mapaTytulow.clear();
        mapaPolWartosci.clear();
        listaWierszy.clear();
        ileSeg = 0;
        listaWierszy.clear();
    }
}

void KartyPostaci::on_actionOtw_rz_triggered()
{
    wyczysc();
    import("");
}

void KartyPostaci::otworz(QString filename)
{
    wyczysc();
    import(filename);
}
